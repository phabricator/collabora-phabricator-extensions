# Collabora Phabricator Extensions

A collection of small customisations to Phabricator.

  - `ManiphestCopyEverybodyMentionedAsSubcribersHeraldAction.php`
    - Adds a new Herald Action for Maniphest tasks and other subtypes which CC's (by adding to subscribers list) anybody mentioned in a custom field which satisfies:
      - `type`: `datasource`
      - `datasource.class`: `PhabricatorPeopleDatasource`

## Installation

Update Phabricator's config thus:

```json
"load-libraries": [
  "collabora-phabricator-extensions": "/path/to/this/repository/",
	...
]
```

