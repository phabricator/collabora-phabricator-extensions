<?php
/**
 * A HeraldAction which iterates through the custom fields on a Maniphest task or subtype
 * and will copy any fields containing people (e.g. approvers) into the subscribers field.
 * The effect being to CC anybody mentioned in a ticket's custom fields
 **/

final class ManiphestCopyEverybodyMentionedAsSubcribersHeraldAction
  extends PhabricatorSubscriptionsHeraldAction {

  const ACTIONCONST = 'maniphest.mentions.add';

  public function getHeraldActionName() {
    return pht('CC (subscribe) anybody mentioned in any field');
  }

  public function supportsRuleType($rule_type) {
    return ($rule_type != HeraldRuleTypeConfig::RULE_TYPE_PERSONAL);
  }

  public function applyEffect($object, HeraldEffect $effect) {
    $phids = array();
    $fields = PhabricatorCustomField::getObjectFields(
        $object,
        PhabricatorCustomField::ROLE_HERALD
    );
    foreach($fields->getFields() as $field) {
        $prox = $field->getProxy();
        if(
            $prox->getFieldType() == 'datasource'
            && $prox->getFieldConfigValue('datasource.class') == 'PhabricatorPeopleDatasource' 
        ) {
            $val = $prox->getFieldValue();
            if(is_array($val)) {
                $phids = array_merge($phids, $prox->getFieldValue());
            }
        }
    }
    return $this->applySubscribe($phids, $is_add = true);
  }

  public function getHeraldActionStandardType() {
    return self::STANDARD_NONE;
  }

  public function renderActionDescription($value) {
    return pht('CC (subscribe) anybody mentioned in any field');
  }

}

// EOF
